import numpy as np


def multiply(a, b):
    rowA = len(a)
    columnA = len(a[0])
    rowB = len(b)
    columnB = len(b[0])

    try:
        answer = [[0]*columnB for i in range(rowA)]
        for i in range(rowA):
            for j in range(columnB):
                for k in range(columnA):
                    answer[i][j] = answer[i][j] + a[i][k] * b[k][j]

        print(answer)
        return answer
    except IndexError:
        print("Inner matrix dimensions must agree")

def multiplyWithNumpy(a, b):
    return np.matmul(a, b)


testdata = [
    ([[3]], [[6]], [[18]]),
    ([[1, 2]],   [[3], [4]], [[11]]),
    ([[1, 2, 3], [4, 5, -7]],  [[3, 0, 4], [2, 5, 1], [-1, -1, 0]],  [[4, 7, 6], [29, 32, 21]])
]
multiplicationFunc = [multiply, multiplyWithNumpy]

def test_multiply(multiplicationFunc, a, b, expected):
    assert np.array_equal(multiplicationFunc(a, b), expected)


testdataImpossibleProducts = [
    ([[3, 4]], [[6, 7]]),
    ([[1, 2]], [[3, 4], [4, 5], [5, 6]])
]

def test_multiplyIncompatibleMatrices(multiplicationFunc, a, b):
    with pytest.raises(Exception) as e:
        multiplicationFunc(a, b)


for func in multiplicationFunc:
    for a, b, expected in testdata:
        test_multiply(func, a, b, expected)

for func in multiplicationFunc:
    for a, b in testdataImpossibleProducts:
        try:
            test_multiply(func, a, b)
            assert False
        except:
            pass
